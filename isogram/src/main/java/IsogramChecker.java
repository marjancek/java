class IsogramChecker {

	/*
	 * An isogram (also known as a "nonpattern word") is a word or phrase without a
	 * repeating letter, however spaces and hyphens are allowed to appear multiple
	 * times.
	 */
	boolean isIsogram(String phrase) {
		return !phrase.matches("^.*([a-zA-Z]).*\1.*");
	}

}
