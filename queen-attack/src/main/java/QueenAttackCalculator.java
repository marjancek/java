public final class QueenAttackCalculator {

	private final Queen q1;
	private final Queen q2;

	public QueenAttackCalculator(Queen q1, Queen q2) {
		if (q1 == null || q2 == null)
			throw new IllegalArgumentException("You must supply valid positions for both Queens.");
		if (q1.getX() == q2.getX() && q1.getY() == q2.getY())
			throw new IllegalArgumentException("Queens cannot occupy the same position.");

		this.q1 = q1;
		this.q2 = q2;
	}

	public boolean canQueensAttackOneAnother() {
		return q1.getX() == q2.getX() || q1.getY() == q2.getY() // vertical or horizontal
				|| (q1.getX() - q1.getY()) == (q2.getX() - q2.getY())  // diagonal up /
				|| (q1.getX() - q2.getY()) == (q2.getX() - q1.getY()); // diagonal down \
	}
}