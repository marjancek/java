import java.util.Arrays;
import java.util.Iterator;

class Proverb {

	private static final String FOR_WHAT = "For want of a %s the %s was lost.\n";
	
	private static final String AND_ALL = "And all for the want of a %s.";
	
	private String result="";
	
    Proverb(String[] words) {
    	if(words.length==0)
    		return;

    	StringBuilder buffer = new StringBuilder();
    	
    	Iterator<String> iterator = Arrays.stream(words).iterator();
    	String first = iterator.next();
    	String previous = first;
    	
    	while(iterator.hasNext()) {
    		String current = iterator.next();
    		buffer.append(String.format(FOR_WHAT, previous, current));
    		previous=current;
    	}
    	
    	buffer.append(String.format(AND_ALL, first));
    	result = buffer.toString();
    }

    String recite() {
        return result;
    }

}
