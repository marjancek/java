class BeerSong {
	 private static final String PLURAL = "%d bottle%s of beer on the wall, %d bottle%s of beer.\nTake one down and pass it around, %d bottle%s of beer on the wall.\n";
	 private static final String ONE = "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n";
	 private static final String ZERO = "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n";


	 String sing(int from, int count) {
		 StringBuilder builder = new StringBuilder();
		 while(count-->0) {
			 switch(from) {
			 case 0: builder.append(ZERO); break;
			 case 1: builder.append(ONE); break;
			 default: builder.append(String.format(PLURAL, from, plurals(from), from, plurals(from), from-1, plurals(from-1)));
			 }
			 builder.append("\n");
			 from--;
		 }
		 return builder.toString();
	 }

	 String singSong() {
		 return sing(99, 100);
	 }

	 String plurals(int i) {
		 return i!=1 ? "s" : "";
	 }
	 
}