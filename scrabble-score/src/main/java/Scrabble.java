import java.util.HashMap;
import java.util.Map;

class Scrabble {

	/** conversion table the values of each letter */
	private static final Map<Integer, Integer> values = new HashMap<>();
	static {
		values.put((int) 'A', 1);
		values.put((int) 'E', 1);
		values.put((int) 'I', 1);
		values.put((int) 'O', 1);
		values.put((int) 'U', 1);
		values.put((int) 'L', 1);
		values.put((int) 'N', 1);
		values.put((int) 'R', 1);
		values.put((int) 'S', 1);
		values.put((int) 'T', 1);
		values.put((int) 'D', 2);
		values.put((int) 'G', 2);
		values.put((int) 'B', 3);
		values.put((int) 'C', 3);
		values.put((int) 'M', 3);
		values.put((int) 'P', 3);
		values.put((int) 'F', 4);
		values.put((int) 'H', 4);
		values.put((int) 'V', 4);
		values.put((int) 'W', 4);
		values.put((int) 'Y', 4);
		values.put((int) 'K', 5);
		values.put((int) 'J', 8);
		values.put((int) 'X', 8);
		values.put((int) 'Q', 10);
		values.put((int) 'Z', 10);
	}

	private final String word;

	Scrabble(String word) {
		this.word = word;
	}

	/** returns the score for the provided word */
	int getScore() {
		return word.toUpperCase().chars().map(c -> values.getOrDefault(c, 0)).sum();
	}

}
