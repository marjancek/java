import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class NucleotideCounterTest {

    @Test
    public void testEmptyDnaStringHasNoNucleotides() {
        NucleotideCounter nucleotideCounter = new NucleotideCounter("");

        Map<Character, Integer> expected = new HashMap<Character, Integer>();
        expected.put('A', 0);
        expected.put('C', 0);
        expected.put('G', 0);
        expected.put('T', 0);
        assertThat(nucleotideCounter.nucleotideCounts()).containsExactlyInAnyOrderEntriesOf(expected);
//            .contains(expected.entrySet().toArray(new MapEntry[0]));
    }

    @Test
    public void testDnaStringHasOneNucleotide() {
        NucleotideCounter nucleotideCounter = new NucleotideCounter("G");

        Map<Character, Integer> expected = new HashMap<Character, Integer>();
        expected.put('A', 0);
        expected.put('C', 0);
        expected.put('G', 1);
        expected.put('T', 0);        
        assertThat(nucleotideCounter.nucleotideCounts()).containsExactlyInAnyOrderEntriesOf(expected);
    }

    @Test
    public void testRepetitiveSequenceWithOnlyGuanine() {
        NucleotideCounter nucleotideCounter = new NucleotideCounter("GGGGGGG");

        Map<Character, Integer> expected = new HashMap<Character, Integer>();
        expected.put('A', 0);
        expected.put('C', 0);
        expected.put('G', 7);
        expected.put('T', 0);
        
        assertThat(nucleotideCounter.nucleotideCounts()).containsExactlyInAnyOrderEntriesOf(expected);
    }

    @Test
    public void testDnaStringHasMultipleNucleotide() {
        NucleotideCounter nucleotideCounter
            = new NucleotideCounter("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC");
        Map<Character, Integer> expected = new HashMap<Character, Integer>();
        expected.put('A', 20);
        expected.put('C', 12);
        expected.put('G', 17);
        expected.put('T', 21);
        
        assertThat(nucleotideCounter.nucleotideCounts()).containsExactlyInAnyOrderEntriesOf(expected);
    }

    @Test
    public void testDnaStringHasInvalidNucleotides() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new NucleotideCounter("AGXXACT"));
    }
}
