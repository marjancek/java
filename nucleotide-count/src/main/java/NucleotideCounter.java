import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

class NucleotideCounter {
	
	private Map<Character, AtomicInteger> counter = new HashMap<Character, AtomicInteger>();
	
	public NucleotideCounter(String rna) {
		if(rna.replaceAll("[ACGT]", "").length()>0)
			throw new IllegalArgumentException("Sequence should only have leters A, C, G and T.");
		
        counter.put('A', new AtomicInteger(0));
        counter.put('C', new AtomicInteger(0));
        counter.put('G', new AtomicInteger(0));
        counter.put('T', new AtomicInteger(0));
        
        rna.chars().forEach(k-> counter.get((char)k).incrementAndGet());
	}
	
	public Map<Character, Integer> nucleotideCounts() {
        Map<Character, Integer> result = new HashMap<Character, Integer>();
        counter.forEach((k, v) -> result.put(k, v.get()));
        return result;
	}
}