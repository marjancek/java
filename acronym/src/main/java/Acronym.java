import java.util.Arrays;
import java.util.stream.Collector;

class Acronym {

	private final String result;
    Acronym(String phrase) {
    	String[] words = phrase.replaceAll("-", " ").replace("_", "").replaceAll("  +", " ").trim().split(" ");
    	result = Arrays.stream(words).map(s -> s.charAt(0)).collect(
	    			// Use our own collector; faster than converting each char to String
	    			Collector.of(
	    			StringBuilder::new,
	    		    StringBuilder::append,
	    		    StringBuilder::append,
	    		    StringBuilder::toString)).toUpperCase();
    }

    String get() {
        return result;
    }

}
