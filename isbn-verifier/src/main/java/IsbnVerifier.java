import java.util.concurrent.atomic.AtomicInteger;

class IsbnVerifier {


    boolean isValid(String stringToVerify) {
    	String verified = stringToVerify.replace("-", "").replace(" ", "");
    	
    	if(verified.length() != 10 
    			|| verified.replaceAll("[0-9]", "").replace("X", "").length() > 0
    			|| ( verified.contains("X") && verified.indexOf("X") != 9) )
    		return false;

    	AtomicInteger factor = new AtomicInteger(10);
    	Integer calc = verified.chars().map(a -> a=='X' ? 10 : a-'0').map(a -> a*factor.getAndDecrement()).sum();
    	return calc%11 == 0;
    }

}
