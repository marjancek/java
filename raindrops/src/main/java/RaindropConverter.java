import java.util.HashMap;
import java.util.Map;

/*
 * Convert a number to a string, the contents of which depend on the number's factors.
 * 
 * If the number has 3 as a factor, output 'Pling'.
 * 
 * If the number has 5 as a factor, output 'Plang'.
 * 
 * If the number has 7 as a factor, output 'Plong'.
 * 
 * If the number does not have 3, 5, or 7 as a factor, just pass the number's digits straight through.
 */
class RaindropConverter {
	// Translation table for the conversion
	private final static Map<Integer, String> mapping = new HashMap<Integer, String>() {
		{
			put(3, "Pling");
			put(5, "Plang");
			put(7, "Plong");
		}
	};

	String convert(int number) {
		StringBuilder builder = new StringBuilder();
		// Find the key that divide the given number, and add their translation to the builder
		mapping.entrySet().stream().filter((e) -> (number % e.getKey()) == 0)
				.forEach(e -> builder.append(e.getValue()));
		// If we didn't find any, the builder will be empty
		return builder.length() > 0 ? builder.toString() : Integer.toString(number);
	}

}
