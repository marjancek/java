import java.util.stream.Collector;

class RotationalCipher {

	private final int shift;
	private static final int WIDTH = 'Z' - 'A' + 1;

	RotationalCipher(int shiftKey) {
		this.shift = shiftKey;
	}

	String rotate(String data) {
		return data.chars()
				.mapToObj(this::mapper)
				.collect(Collector.of(
						StringBuilder::new, 
						StringBuilder::append,
						StringBuilder::append,
						StringBuilder::toString));
	}

	private char mapper(int c) {
		int result = c;
		if (c >= 'A' && c <= 'Z')
			result = shift('A', c);
		else if (c >= 'a' && c <= 'z')
			result = shift('a', c);
		return (char) result;
	}

	private int shift(int base, int c) {
		return base + ((c - base + shift) % WIDTH);
	}
}
