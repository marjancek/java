class RomanNumerals {
	
	private String roman="";
	
	private static final String[] letters = {"I", "V", "X", "L", "C", "D", "M", "", ""};
	
	public RomanNumerals(int num) {
		int i=0;
		while(num>0 && i<=letters.length-3) {
			roman = digit2Roman(num%10, letters[i], letters[i+1], letters[i+2]) + roman;
			num = num/10;
			i+=2;
		}
	}
	
	public String digit2Roman(int digit, String unit, String penta, String deca) {
		String result = "";		
		switch(digit) {
			case 3:	result+=unit;
			case 2:	result+=unit;
			case 1:	result+=unit; break;
			case 4:	result+=unit;
			case 5:	result+=penta; break;
			case 8:	result+=unit; 
			case 7:	result+=unit;  
			case 6:	result=penta+unit+result;  break;
			case 9:    result+=unit+deca;
		}
		return result;
	}
	
	public String getRomanNumeral() {
		return roman;
	}
}