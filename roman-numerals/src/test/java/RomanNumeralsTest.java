import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RomanNumeralsTest {

    private RomanNumerals romanNumeral;

    @Test
    public void test1ToRomanNumberI() {
        romanNumeral = new RomanNumerals(1);
        assertEquals("I", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test2ToRomanNumberII() {
        romanNumeral = new RomanNumerals(2);
        assertEquals("II", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test3ToRomanNumberIII() {
        romanNumeral = new RomanNumerals(3);
        assertEquals("III", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test4ToRomanNumberIV() {
        romanNumeral = new RomanNumerals(4);
        assertEquals("IV", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test5ToRomanNumberV() {
        romanNumeral = new RomanNumerals(5);
        assertEquals("V", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test6ToRomanNumberVI() {
        romanNumeral = new RomanNumerals(6);
        assertEquals("VI", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test9ToRomanNumberIX() {
        romanNumeral = new RomanNumerals(9);
        assertEquals("IX", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test27ToRomanNumberXXVII() {
        romanNumeral = new RomanNumerals(27);
        assertEquals("XXVII", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test48ToRomanNumberXLVIII() {
        romanNumeral = new RomanNumerals(48);
        assertEquals("XLVIII", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test49ToRomanNumberXLIX() {
        romanNumeral = new RomanNumerals(49);
        assertEquals("XLIX", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test59ToRomanNumberLIX() {
        romanNumeral = new RomanNumerals(59);
        assertEquals("LIX", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test93ToRomanNumberXCIII() {
        romanNumeral = new RomanNumerals(93);
        assertEquals("XCIII", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test141ToRomanNumberCXLI() {
        romanNumeral = new RomanNumerals(141);
        assertEquals("CXLI", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test163ToRomanNumberCLXIII() {
        romanNumeral = new RomanNumerals(163);
        assertEquals("CLXIII", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test402ToRomanNumberCDII() {
        romanNumeral = new RomanNumerals(402);
        assertEquals("CDII", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test575ToRomanNumberDLXXV() {
        romanNumeral = new RomanNumerals(575);
        assertEquals("DLXXV", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test911ToRomanNumberCMXI() {
        romanNumeral = new RomanNumerals(911);
        assertEquals("CMXI", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test1024ToRomanNumberMXXIV() {
        romanNumeral = new RomanNumerals(1024);
        assertEquals("MXXIV", romanNumeral.getRomanNumeral());
    }

    @Test
    public void test3000ToRomanNumberMMM() {
        romanNumeral = new RomanNumerals(3000);
        assertEquals("MMM", romanNumeral.getRomanNumeral());
    }

}
