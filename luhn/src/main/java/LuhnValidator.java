class LuhnValidator {

    boolean isValid(String candidate) {
    	char[] trimmed = candidate.replaceAll("\\s", "").toCharArray();
    	char[] clean= candidate.replaceAll("[^0-9]", "").toCharArray();
    	if(clean.length<=1 || trimmed.length!=clean.length)
    		return false;
    	
    	int accum=0;
    	for(int i=0; i<clean.length; i++) 
    		accum +=  toInt(clean[i], (clean.length-i)%2==0);
    	
    	return accum%10==0;
    }

    private int toInt(char c, boolean duplicate) {
    	int i = c-'0';
    	return duplicate ? pseudoDouble(i) : i;
    }
    
    private int pseudoDouble(int i) {
    	return i > 4 ? i*2 -9 : i*2;
    }
}
