import java.time.LocalDate;
import java.time.LocalDateTime;

public class Gigasecond {

	public static final long GIGA_SECONDS = 1_000_000_000L;
	private final LocalDateTime time;

	public Gigasecond(final LocalDate moment) {
		this(moment.atStartOfDay());
	}

	public Gigasecond(final LocalDateTime moment) {
		this.time = moment.plusSeconds(GIGA_SECONDS);
	}

	public LocalDateTime getDateTime() {
		return time;
	}
}
