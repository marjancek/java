import java.util.Arrays;

class Matrix {

	private final int[][] m;

	Matrix(String matrixAsString) {
		m = Arrays.stream(matrixAsString.split("\n"))
				.map(Matrix::parseRow)
				.toArray(int[][]::new);
	}

	private static int[] parseRow(String row) {
		return Arrays.stream((row.split(" ")))
				.mapToInt(Integer::valueOf)
				.toArray();
	}

	int[] getRow(int rowNumber) {
		return m[rowNumber - 1].clone();
	}

	int[] getColumn(int columnNumber) {
		return Arrays.stream(m)
				.mapToInt(r -> r[columnNumber - 1])
				.toArray();
	}
}
