import java.util.Arrays;
import java.util.List;

class ResistorColorDuo {
	
	private static final String[] colours = {"black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"};
	private static final List<String> colourList = Arrays.asList(colours);
	
    int value(String[] colors) {
    	// we are guaranteed to always have at least two, and we only care for the first two, so...
        return colourList.indexOf(colors[0])*10+colourList.indexOf(colors[1]);
    }
}
