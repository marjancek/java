import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

class DiamondPrinter {

	private static final char first = 'A';
	
    List<String> printToList(char a) {
    	LinkedList<String> list = new LinkedList<>();
    	// Calculate the width of each line
    	int width = (a - first)*2 +1;
    	// Do the middle one independently, since its only one
    	list.add(level(a, width, 0));
    	char current = a;
    	// Do for all the letter until inclusively the A
    	while(first<=--current) {
    		// add each letter level both before and after
    		String line = level(current, width, a - current);
    		list.addFirst(line);
    		list.addLast(line);   		
    	}
    	return list;
    }
    
    private String level(char a, int width, int left) {
    	// calculate length of central segment
    	int centre = width -left -left -2;
    	String side = makePointLine(left);
    	if(centre<=0) {
    		return   side + a + side;
    	} else {
    		String middle =  makePointLine(centre);
    		return   side + a + middle + a + side;
    	}
    }

    private String makePointLine(int n) {
    	return Collections.nCopies(n, " ").stream().collect(Collectors.joining());
    }
}
