class Darts {

	private final static double OUTER = 10;
	private final static double MIDDLE = 5;
	private final static double INNER = 1;
	private final int points;
	
    Darts(double x, double y) {
    	// Avoid root by comparing to squared distance
    	double distance2 = x*x + y*y;
    	if(distance2 <= INNER*INNER)
    		points=10;
    	else if(distance2 <= MIDDLE*MIDDLE)
    		points = 5;
    	else if(distance2 <=OUTER*OUTER)
    		points = 1;
    	else
    		points = 0;
    }

    int score() {
    	return points;
	}

}
