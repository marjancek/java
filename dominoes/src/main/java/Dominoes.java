import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

class Dominoes {

	public List<Domino> formChain(List<Domino> dominoesList) throws ChainNotFoundException {
		Optional<LinkedList<Domino>> result = Optional.of(new LinkedList<>());
		if (!dominoesList.isEmpty()) {
			result = complete(dominoesList.get(0).getLeft(), new LinkedList<Domino>(dominoesList.subList(0, 1)),
					new LinkedList<Domino>(dominoesList.subList(1, dominoesList.size())), new LinkedList<Domino>());
		}
		if (!result.isPresent()) {
			throw new ChainNotFoundException("No domino chain found.");
		}
		return result.get();
	}

	Optional<LinkedList<Domino>> complete(int start, LinkedList<Domino> sorted, LinkedList<Domino> unsorted,
			LinkedList<Domino> discarted) {
		// If we run out of stones to take, then we completed the longest path we could take down this road
		if (unsorted.isEmpty()) {
			if (discarted.isEmpty() && start == sorted.getLast().getRight())
				return Optional.of(sorted);
			else
				return Optional.empty();
		} else {
			Optional<LinkedList<Domino>> found;
			// We will change, but then restore three collections (remove->add, add->remove)
			// take first unused stone
			Domino next = unsorted.removeFirst();
			// if it fits to the path we already generated
			if (next.getLeft() == sorted.getLast().getRight()) {
				sorted.addLast(next); // add it to the path
				unsorted.addAll(discarted); // and consider all stones, even discarded, further down
				found = (complete(start, sorted, unsorted, new LinkedList<>()));
				if (found.isPresent())
					return found;	// we found a solution; get out of here before restoring anything!
				unsorted.removeAll(discarted); // restore the unused stones
				sorted.removeLast(); // remove the stone added in this step (it was not good)
			} else if (next.getRight() == sorted.getLast().getRight()) {
				sorted.addLast(new Domino(next.getRight(), next.getLeft()));
				unsorted.addAll(discarted);
				found = (complete(start, sorted, unsorted, new LinkedList<>()));
				if (found.isPresent())
					return found;
				unsorted.removeAll(discarted);
				sorted.removeLast();
			}
			// So the stone doesn't fit; let's discard it, and move on
			discarted.addLast(next);
			found = (complete(start, sorted, unsorted, discarted));
			if (found.isPresent())
				return found;  // if we found it; return right away!
			discarted.removeLast(); // take back the discarded stone
			unsorted.addFirst(next); // put back the unused stone before leaving
			return found;
			// all collections are in the same state they started
		}
	}

}