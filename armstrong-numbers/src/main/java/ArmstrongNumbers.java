class ArmstrongNumbers {

    boolean isArmstrongNumber(int numberToCheck) {
    	String num = String.valueOf(numberToCheck);
    	return num.chars().map(n -> 	(int) Math.pow(n-'0', 	num.length())).sum() == numberToCheck;
    }

}
