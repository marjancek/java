import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

class WordSearcher {
	// all 8 directions
	private static final List<Pair> DIR = Arrays.asList(new Pair(0, 1), new Pair(1, 1), new Pair(1, 0), new Pair(1, -1),
			new Pair(0, -1), new Pair(-1, -1), new Pair(-1, 0), new Pair(-1, 1));

	public Map<String, Optional<WordLocation>> search(Set<String> words, char[][] m) {
		return words.stream().collect(Collectors.toMap(Function.identity(), w -> this.search(w, m)));
	}

	private Optional<WordLocation> search(String word, char[][] m) {
		Pair max = new Pair(m[0].length, m.length);
		for (int i = 1; i <= m.length; i++)
			for (int j = 1; j <= m[0].length; j++)
				if (m[i - 1][j - 1] == word.charAt(0))
					for (Pair dir : DIR) {
						Pair from = new Pair(j, i);
						Pair to = from.plus(dir.times(word.length() - 1));
						if (to.within(max) && canTrace(word.toCharArray(), from, dir, m))
							return Optional.of(new WordLocation(from, to));
					}
		return Optional.empty();
	}

	private boolean canTrace(char[] word, Pair start, Pair dir, char m[][]) {
		Pair c = start;
		for (int i = 0; i < word.length; i++) {
			if (m[c.getY() - 1][c.getX() - 1] != word[i])
				return false;
			c = c.plus(dir);
		}
		return true;
	}

}