import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Matrix {

	private final Set<MatrixCoordinate> result = new HashSet<>();;

	Matrix(List<List<Integer>> values) {
		int nrows = values.size();
		int ncols = values.isEmpty() ? 0 : values.get(0).size();
		final int[] maxRows = new int[nrows]; // highest value of each row
		final int[] minColumns = new int[ncols]; // lowest value of each column
		Arrays.fill(maxRows, Integer.MIN_VALUE);
		Arrays.fill(minColumns, Integer.MAX_VALUE);

		// calculate the maxima of each row, and the minima of each columns
		for (int i = 0; i < nrows; i++)
			for (int j = 0; j < ncols; j++) {
				int current = values.get(i).get(j);
				maxRows[i] = maxRows[i] > current ? maxRows[i] : current;
				minColumns[j] = minColumns[j] < current ? minColumns[j] : current;
			}

		// If the candidates for rows and column match, then its a saddle point
		for (int i = 0; i < maxRows.length; i++)
			for (int j = 0; j < minColumns.length; j++)
				if (maxRows[i] == minColumns[j])
					result.add(new MatrixCoordinate(i + 1, j + 1));
	}

	Set<MatrixCoordinate> getSaddlePoints() {
		// return copy, to avoid exposing Set
		return new HashSet<>(result);
	}
}
