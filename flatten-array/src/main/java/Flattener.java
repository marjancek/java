import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Flattener {

	public List<Object> flatten(List<Object> list) {
		return list.stream().flatMap(e -> listify(e).stream()).collect(Collectors.toList());
	}

	public List<Object> listify(Object obj) {
		if (obj instanceof List)
			return flatten((List<Object>) obj);
		else if (obj == null)
			return new ArrayList<>();
		else
			return Arrays.asList(obj);
	}
}