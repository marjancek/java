import java.util.stream.IntStream;

class NaturalNumber {
	private int n;
	private int sum;

	public NaturalNumber(int n) {
		if (n <= 0)
			throw new IllegalArgumentException("You must supply a natural number (positive integer)");
		this.n = n;
		this.sum = IntStream.rangeClosed(1, n / 2).filter(x -> n % x == 0).sum();
	}

	public Classification getClassification() {
		if (n < sum)
			return Classification.ABUNDANT;
		else if (n > sum)
			return Classification.DEFICIENT;
		else
			return Classification.PERFECT;
	}

}
