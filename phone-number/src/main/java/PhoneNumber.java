class PhoneNumber {
	
	private  String digits;
	
	public PhoneNumber(String number) {
		digits = number.replaceAll("[^0-9]", "");
		
		if(number.replaceAll("[^a-zA-Z]", "").length()>0)
			throw new IllegalArgumentException("letters not permitted");
		if(number.replaceAll("[0-9\\(\\)\\-\\s\\+\\.]", "").length()>0)
			throw new IllegalArgumentException("punctuations not permitted");
		
		if(digits.length()>11)
			throw new IllegalArgumentException("more than 11 digits");
		if(digits.length()==11) {
			if(digits.charAt(0) != '1')
				throw new IllegalArgumentException("11 digits must start with 1");
			digits = digits.substring(1); // remove country code
		}
		
		if(digits.length()!=10)
			throw new IllegalArgumentException("incorrect number of digits");
		if(digits.charAt(0) == '1')
			throw new IllegalArgumentException("area code cannot start with one");
		if(digits.charAt(0) == '0')
			throw new IllegalArgumentException("area code cannot start with zero");
		if(digits.charAt(3)=='0')
			throw new IllegalArgumentException("exchange code cannot start with zero");
		if(digits.charAt(3)=='1')
			throw new IllegalArgumentException("exchange code cannot start with one");
	}
	
	public String getNumber() {
		return digits;
	}
}