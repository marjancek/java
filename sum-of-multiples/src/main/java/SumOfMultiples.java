import java.util.stream.IntStream;

class SumOfMultiples {

	private final int number;
	private int[] set;

	SumOfMultiples(int number, int[] set) {
		this.number = number;
		this.set = set;
	}

	private boolean hasDivisorIgnoreZero(int number) {
		return IntStream.of(set).filter(x -> x != 0).anyMatch(y -> number % y == 0);
	}

	int getSum() {
		return IntStream.range(1, number).filter(this::hasDivisorIgnoreZero).sum();
	}

}
