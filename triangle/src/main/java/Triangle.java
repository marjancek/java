import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Triangle {

	private final List<Double> sides;

	Triangle(double side1, double side2, double side3) throws TriangleException {
		sides = Arrays.asList(side1, side2, side3);
		Collections.sort(sides);

		if (!sides.stream().allMatch(s -> s > 0.0) || sides.stream().mapToDouble(d -> d).sum() / 2 <= sides.get(2))
			throw new TriangleException();
	}

	boolean isEquilateral() {
		return sides.stream().distinct().count() == 1;
	}

	boolean isIsosceles() {
		return sides.stream().distinct().count() <= 2;
	}

	boolean isScalene() {
		return sides.stream().distinct().count() == 3;
	}

}
