import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
*
*  Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of the word.  Please note that 
*  "xr" and "yt" at the beginning of a word make vowel sounds (e.g. "xray" -> "xrayay", "yttria" -> "yttriaay").
*
*  Rule 2: If a word begins with a consonant sound, move it to the end of the word and then add an "ay" sound to the end
*  of the word. Consonant sounds can be made up of multiple consonants, a.k.a. a consonant cluster (e.g. "chair" -> "airchay").
*
*  Rule 3: If a word starts with a consonant sound followed by "qu", move it to the end of the word, and then add an "ay" sound 
*  to the end of the word (e.g. "square" -> "aresquay").
*
*  Rule 4: If a word contains a "y" after a consonant cluster or as the second letter in a two letter word it makes a vowel sound 
*  (e.g. "rhythm" -> "ythmrhay", "my" -> "ymay").
*/

class PigLatinTranslator {

	private static final List<String> vowellets =  Arrays.asList("a", "i", "u", "e", "o", "xr", "yt");
	private static final List<String> consonants =  Arrays.asList("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "qu", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	private static final String AY = "ay";
	
	public static String translateWord(String plain) {
		Optional<String> rule1 = vowellets.stream().filter(a -> plain.startsWith(a)).findFirst();
		Optional<String> rule2 = consonants.stream().filter(a -> plain.startsWith(a)).findFirst();
		
		// If the first one is a consonant, and is followed by 'qu', then grab the three characters
		boolean hasQu = plain.matches("^.qu.*");
		String rule3 = rule2.isPresent() && hasQu ? plain.substring(0, 3) : "";
		
	    Optional<Integer> firstVowel = vowellets.stream().map(a->plain.indexOf(a)).sorted().filter((n) -> (n>0)).findFirst();	
	    String rule4a = firstVowel.isPresent() && firstVowel.get()>1 ? plain.substring(0, firstVowel.get()): "";
	    String rule4b = rule1.isPresent() && plain.charAt(1)=='y' && plain.length()==2 ? rule1.get() : "";
	    String rule4 = rule4b.isEmpty() ? rule4a : rule4b;
	    
	    if(rule1.isPresent() ) {
	    	return plain+AY;
	    } else if (!rule3.isEmpty()) {
	    	return plain.substring(3) + rule3 + AY;
	    } else if (!rule4.isEmpty()) {
	    	return plain.substring(rule4.length()) + rule4 + AY;
	    } else if (rule2.isPresent() ) {
	    	return plain.substring(rule2.get().length()) + rule2.get() + AY;
	    } 
		return plain;
	}
	
	public  String translate(String plain) {
		return Stream.of(plain.split(" ")).map(PigLatinTranslator::translateWord).collect(Collectors.joining(" "));
	}

}