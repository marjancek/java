import java.util.HashMap;
import java.util.Map;

public class WordCount {

	public Map<String, Integer> phrase(String text) {
		String[] res = text.toLowerCase()
				.replaceAll("\\'\\s|\\'$|\\s\\'|^\\'", " ")
				.split("\\'*[\\s\\n,:;!?&@\\$%\\^\\.]+\\'*");
		
		Map<String, Integer> result = new HashMap<>();
		for (String key : res) {
			if (!key.isEmpty())
				result.put(key, result.getOrDefault(key, 0) + 1);
		}
		
		return result;
	}
}