import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class Poker {
	private List<PokerHand> all;

	public Poker(List<String> hands) {
		all = hands.stream().map(s -> (new PokerHand(s))).collect(Collectors.toList());
	}

	public List<String> getBestHands() {
		Optional<PokerHand> best = all.stream().max(PokerHand::compareTo);
		if (!best.isPresent())
			return new ArrayList<>();
		// return list of all best hands (ties)
		return all.stream().filter(o -> best.get().compareTo(o) == 0).map(PokerHand::toString)
				.collect(Collectors.toList());
	}
}