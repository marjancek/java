import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.function.Function;

public class PokerHand implements Comparable<PokerHand> {

	private final List<Integer> ranks; // ordered numbers
	private final Map<Integer, Long> groupedRanks; // grouped by number
	private final int level; // high-level score
	private final String original;

	public PokerHand(String cards) {
		original = cards;
		ranks = Arrays.stream(cards.split(" ")).map(this::cardNumber).sorted().collect(Collectors.toList());
		groupedRanks = ranks.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		level = getLevel();
	}

	private int getBestGroupOf(int n) {
		return groupedRanks.entrySet().stream().filter(e -> e.getValue() == n).map(Entry::getKey)
				.max(Integer::compareTo).orElse(0);
	}

	private long getNumberOfPairs() {
		return groupedRanks.entrySet().stream().filter(e -> e.getValue() == 2).count();
	}

	private int getLowerPair() {
		return groupedRanks.entrySet().stream().filter(e -> e.getValue() == 2).map(Entry::getKey)
				.min(Integer::compareTo).orElse(0);
	}

	private boolean isLowStraight() { // Straight with Ace used as one
		return groupedRanks.size() == 5 && ranks.get(0) == 2 && ranks.get(3) == 5 && ranks.get(4) == 14;
	}

	private boolean isStraight() {
		return groupedRanks.size() == 5 && ranks.get(0) + 4 == ranks.get(4);
	}

	private boolean isFlush() {
		return Arrays.stream(original.split(" ")).map(this::suit).distinct().count() == 1;
	}

	private boolean isFullHouse() {
		return getNumberOfPairs() == 1 && getBestGroupOf(3) > 0;
	}

	private int getLevel() {
		if (getBestGroupOf(5) > 0)
			return 11;
		if (isFlush() && isStraight())
			return 10;
		if (isFlush() && isLowStraight())
			return 9;
		if (getBestGroupOf(4) > 0)
			return 8;
		if (isFullHouse())
			return 7;
		if (isFlush())
			return 6;
		if (isStraight())
			return 5;
		if (isLowStraight())
			return 4;
		if (getBestGroupOf(3) > 0)
			return 3;
		if (getNumberOfPairs() == 2)
			return 2;
		if (getNumberOfPairs() == 1)
			return 1;
		return 0;
	}

	@Override
	public int compareTo(PokerHand other) {
		int diff = this.level - other.level;
		if (diff != 0)
			return diff;
		switch (level) {
		case 4: // stairs
		case 5:
		case 9:
		case 10:
		case 0: // non-stairs
		case 6:
		case 11:
			return compareHighest(other);
		case 1: // pairs
		case 2:
			return comparePairs(other);
		case 3: // triples
		case 7:
			return compareTriple(other);
		case 8: // quads
			return compareQuads(other);
		default:
			return 0;
		}
	}

	private int compareQuads(PokerHand other) {
		int diff = this.getBestGroupOf(4) - other.getBestGroupOf(4);
		if (diff != 0)
			return diff;
		return compareHighest(other);
	}

	private int compareTriple(PokerHand other) {
		int diff = this.getBestGroupOf(3) - other.getBestGroupOf(3);
		if (diff != 0)
			return diff;
		return compareHighest(other);
	}

	private int comparePairs(PokerHand other) {
		int diff = this.getBestGroupOf(2) - other.getBestGroupOf(2);
		if (diff != 0)
			return diff;
		diff = this.getLowerPair() - other.getLowerPair();
		if (diff != 0)
			return diff;
		return compareHighest(other);
	}

	private int compareHighest(PokerHand other) {
		for (int i = 4; i >= 0; i--) {
			int diff = ranks.get(i) - other.ranks.get(i);
			if (diff != 0)
				return diff;
		}
		return 0;
	}

	private char suit(String card) {
		return card.charAt(card.length() - 1);
	}

	private int cardNumber(String card) {
		String r = card.substring(0, card.length() - 1);
		switch (r) {
		case "A":
			return 14;
		case "K":
			return 13;
		case "Q":
			return 12;
		case "J":
			return 11;
		case "10":
			return 10;
		case "9":
		case "8":
		case "7":
		case "6":
		case "5":
		case "4":
		case "3":
		case "2":
			return r.charAt(0) - '0';
		default:
			throw new InvalidParameterException("Unknown rank " + r);
		}
	}

	@Override
	public String toString() {
		return original;
	}
}