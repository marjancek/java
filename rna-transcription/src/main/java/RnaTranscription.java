class RnaTranscription {

    String transcribe(String dnaStrand) {
    	return dnaStrand.replace("G", "x").replace("C", "G").replace("x","C").replace("A","U").replace("T","A");
    }
}
