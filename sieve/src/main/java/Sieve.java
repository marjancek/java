import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Sieve {
	private List<Integer> list;

	Sieve(int maxPrime) {
		list = IntStream.rangeClosed(2, maxPrime).boxed().collect(Collectors.toList());
		for (int i = 0; i < list.size(); i++) {
			final int p = list.get(i);
			list.removeAll(list.stream().filter(x -> (p != x) && (x % p == 0)).collect(Collectors.toList()));
		}
	}

	List<Integer> getPrimes() {
		return list;
	}
}
