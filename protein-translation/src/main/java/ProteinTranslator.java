import java.util.ArrayList;
import java.util.List;

class ProteinTranslator {

	public enum Protein {
		Methionine,
		Phenylalanine,
		Leucine,
		Serine,
		Tyrosine,
		Cysteine,
		Tryptophan,
		STOP;
	}

	public enum Codon {
		AUG(Protein.Methionine),
		UUU(Protein.Phenylalanine),
		UUC(Protein.Phenylalanine),
		UUA(Protein.Leucine),
		UUG(Protein.Leucine),
		UCU(Protein.Serine),
		UCC(Protein.Serine),
		UCA(Protein.Serine),
		UCG(Protein.Serine),
		UAU(Protein.Tyrosine),
		UAC(Protein.Tyrosine),
		UGU(Protein.Cysteine),
		UGC(Protein.Cysteine),
		UGG(Protein.Tryptophan),
		UAA(Protein.STOP),
		UAG(Protein.STOP),
		UGA(Protein.STOP);
		
		private Protein p;
		private Codon(Protein p) {
				this.p=p;
		}
		public Protein getProtein() {
			return p;
		}
	}

    List<String> translate(String rnaSequence) {
    	List<String> proteins= new ArrayList<String>();
    	for(int i =0 ; i<rnaSequence.length()-2;i+=3 ){
    		Codon codon = Codon.valueOf(rnaSequence.substring(i, i+3));
    		if(codon.getProtein().equals(Protein.STOP)) {
    			break;
    		}
    		proteins.add(codon.getProtein().name());
    	}
    	return proteins;
    }
}
