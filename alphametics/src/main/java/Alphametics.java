import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Solution optimized for speed
 * 
 * Uses vector of digits to iterate through possible solutions, avoiding
 * recursion (for speed).
 * 
 * Digits {1,2,3} become potential solution map(A->1, B->2, C->3).
 * 
 * As if they were natural numbers, we calculate the successor of a set of
 * digits by adding one to the last digit, and overflowing it if a) it goes over
 * 9, or b) it already exists prior to it.
 * 
 * Thus next(3, 4, 0, 1, 2, 9, 8) -> (3, 4, 0, 1, 5, 2, 6).
 * 
 * When we want to increase 8 it overflows (9 is already used), and then we need
 * to increase the 9, which also overflows, then when increase 2 we skips 3 and
 * 4 (already used) up to 5. Now, what were 9 and 8 get their lowest possible
 * values.
 * 
 */
class Alphametics {

	private final String result;
	private final String[] terms;
	private List<Character> chars;
	private List<Character> firsts;

	public Alphametics(String ecuation) {
		String[] parts = ecuation.split("\\s==\\s");
		result = parts[1];
		terms = parts[0].split("\\s\\+\\s");

		Set<Character> charSet = stringToSet(result); // unique chars
		Arrays.stream(terms).forEach(s -> charSet.addAll(stringToSet(s)));
		chars = charSet.stream().collect(Collectors.toList());

		Set<Character> firstsSet = new HashSet<>(); // unique chars
		firstsSet.add(result.charAt(0));
		Arrays.stream(terms).forEach(s -> firstsSet.add(s.charAt(0)));
		firsts = firstsSet.stream().collect(Collectors.toList());
	}

	public Map<Character, Integer> solve() throws UnsolvablePuzzleException {
		int[] nums = IntStream.range(0, chars.size()).toArray();
		Map<Character, Integer> map = updateMap(new HashMap<>(), nums);
		// scan all possible combinations until one is a solution; or none left
		while (nums != null && !isSolution(map)) {
			nums = next(nums); // try next 'numbers' as solution
			if (nums == null) {
				throw new UnsolvablePuzzleException();
			}
			updateMap(map, nums);
		}

		return map;
	}

	private boolean isSolution(Map<Character, Integer> map) {
		int resultVal = wordValue(result, map);
		int total = Arrays.stream(terms).mapToInt(w -> wordValue(w, map)).sum();
		// no first letters is a zero, and the addition equals the result value
		return firsts.stream().noneMatch(c -> map.get(c) == 0) && total == resultVal;
	}

	private Map<Character, Integer> updateMap(Map<Character, Integer> map, int[] nums) {
		// nums{1,2,3} -> map(A->1, B->2, C->3)
		for (int i = 0; i < chars.size(); i++) {
			map.put(chars.get(i), nums[i]);
		}
		return map;
	}

	// Avoid recursion by calculating next; as in natural numbers next(199)->200
	private int[] next(int[] nums) {
		for (int i = nums.length - 1; i >= 0; i--) {
			nums[i] = smallestBiggerAvailable(nums[i] + 1, nums, i);
			if (nums[i] <= 9)
				return filllSmallests(nums, i + 1);
		}
		return null;
	}

	// after overflow: 23987-> 24013
	private int[] filllSmallests(int[] nums, int from) {
		for (int i = from; i < nums.length; i++) {
			nums[i] = smallestBiggerAvailable(0, nums, i);
		}
		return nums;
	}

	// 3, 4, 0, 5, 2, [3] -> 3, 4, 0, 5, 2, 6
	private int smallestBiggerAvailable(int atLeast, int[] nums, int upto) {
		int last;
		do {
			last = atLeast;
			for (int i = 0; i < upto; i++) {
				if (nums[i] == atLeast)
					atLeast++;
			}
		} while (last != atLeast);
		return atLeast;
	}

	private Set<Character> stringToSet(String s) {
		return s.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
	}

	// ((A*10)+B)*10+C = A*100+B*10+C
	private int wordValue(String word, Map<Character, Integer> map) {
		char[] letters = word.toCharArray();
		int tot = 0;
		for (int i = 0; i < letters.length; i++) {
			tot = tot * 10 + map.get(letters[i]);
		}
		return tot;
	}

}