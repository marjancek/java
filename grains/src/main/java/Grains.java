import java.math.BigInteger;

class Grains {

	private static final BigInteger base = new BigInteger("2");
	
    BigInteger grainsOnSquare(final int square) {
    	if(square>64 || square <1) 
    		throw new IllegalArgumentException("square must be between 1 and 64");
    	// P[i=1..n](2 ^i) = 2^(n-1)
    	return base.pow(square-1);
    }

    BigInteger grainsOnBoard() {
    	// sum[i=1..n](2^i) = (2^n)-1
    	return base.pow(64).subtract(BigInteger.ONE);
    }

}
