import java.util.ArrayList;
import java.util.List;

class KindergartenGarden {

    private String[] split;

	KindergartenGarden(String garden) {
    	split = garden.split("\n");
    	
    }

    List<Plant> getPlantsOfStudent(String student) {
        List<Plant> plants = new ArrayList<>();
    	int i = student.getBytes()[0]-'A';
    	for(String line:split) {
        	plants.add(Plant.getPlant(line.charAt(i*2)));
        	plants.add(Plant.getPlant(line.charAt(i*2+1)));
    	}
        return plants;
    }

}
