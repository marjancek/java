import java.util.List;
import java.util.stream.Collectors;

class LargestSeriesProductCalculator {
	private final List<Long> input;
	
    LargestSeriesProductCalculator(String inputNumber) {
    	if(inputNumber.length()!=inputNumber.replaceAll("[^0-9]", "").length()   )
    		throw new IllegalArgumentException ("String to search may only contain digits.");
    	input =  inputNumber.chars().mapToObj(a -> (long) (a-'0')).collect(Collectors.toList());	
    }

    long calculateLargestProductForSeriesLength(int numberOfDigits) {
    	if(numberOfDigits>input.size() )
    		throw new IllegalArgumentException ("Series length must be less than or equal to the length of the string to search.");
    	if(numberOfDigits <0 )
    		throw new IllegalArgumentException ("Series length must be non-negative.");
    	if(numberOfDigits==0)
    		return 1;
    	
    	long biggest = 0;
    	for(int i = 0; i<=input.size()-numberOfDigits; i++) {
    		long prod =  input.stream().skip(i).limit(numberOfDigits).reduce(1L, (a, b) -> a*b);
    		biggest = biggest < prod ? prod : biggest;
    	}
    	
    	return biggest;
    }
    
}
