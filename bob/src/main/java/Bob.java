class Bob {
	
	private static String NORMAL= "Whatever.";
	private static String YELL = "Whoa, chill out!";
	private static String ASK = "Sure.";
	private static String YELL_QUESTION= "Calm down, I know what I'm doing!";
	private static String  EMPTY = "Fine. Be that way!";
	
	public String hey(String message) {
		String msg = message.replaceAll("\\s", "");
		boolean hasLetters = msg.chars().filter(Character::isAlphabetic).count() >0;
		boolean allCaps = hasLetters && msg.chars().filter(Character::isAlphabetic).allMatch(Character::isUpperCase);
		boolean question = msg.endsWith("?");
		
		if(msg.isEmpty()) { 
			return EMPTY;
		}else if(allCaps) {
			if(question)
				return YELL_QUESTION;
			else
				return YELL;
		} else if (question) {
			return ASK;
		} 		
		return NORMAL;
	}	
}