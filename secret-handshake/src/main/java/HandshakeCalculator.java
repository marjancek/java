import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class HandshakeCalculator {
	List<Signal> calculateHandshake(int number) {
		final Stream<Signal> stream = Stream.of(Signal.values()).filter(s -> ((1 << (s.ordinal()) & number) > 0));
		if ((number & 16) > 0)
			return stream.sorted(Collections.reverseOrder()).collect(Collectors.toList());
		else
			return stream.collect(Collectors.toList());
	}
}
