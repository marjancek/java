import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Given a word and a list of possible anagrams, select the correct sublist.
 * Given "listen" and a list of candidates like "enlists" "google" "inlets" "banana" 
 * the program should return a list containing "inlets".
*/

class Anagram {

	private final String root;
	
	public Anagram(String root) {
		// store already lower case
		this.root=root.toLowerCase();
	}

	public List<String> match(List<String> asList) {
		// keep only matching anagrams
		return asList.stream().filter(this::matches).collect(Collectors.toList());
	}
	
	private boolean matches(String word) {
		// Don't accept identical words
		if(root.equalsIgnoreCase(word)) {
			return false;
		}
		// create lower-case iterators with the sorted letters, so anagrams will result in the same representation.
		Iterator<Integer> roots = root.chars().sorted().iterator();
		Iterator<Integer> words = word.toLowerCase().chars().sorted().iterator();
		
		// all elements...
		while(roots.hasNext() && words.hasNext() ) {
			if(roots.next()!=words.next()) {
				return false;
			}
		}
		// ...and length must match
		return !roots.hasNext() && !words.hasNext();
	}
}