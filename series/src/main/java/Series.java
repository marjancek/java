import java.util.ArrayList;
import java.util.List;

class Series {
	
	private String sequence;
	
	public Series(String sequence) {
		this.sequence = sequence;
	}
	
	public List<String> slices(int length){
		
		if(0>=length)
			throw new IllegalArgumentException("Slice size is too small.");
		if(sequence.length()<length)
			throw new IllegalArgumentException("Slice size is too big.");
		
		
		List<String> sliced = new ArrayList<String>();
		for(int i=0; i<=sequence.length()-length;i++)
			sliced.add(sequence.substring(i, i+length));
		return sliced;
	}
}