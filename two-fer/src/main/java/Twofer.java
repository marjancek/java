import java.util.Objects;

/*
 * Given a name, return a string with the message:
 * 
 * One for X, one for me.
 *
 * Where X is the given name.
 * However, if the name is missing, return the string:
 *
 * One for you, one for me.
 */

public class Twofer {

	private static final String BEGGINING = "One for ";
	private static final String END =  ", one for me.";
	private static final String YOU = "you";

	public String twofer(String name) {
		return BEGGINING + (Objects.isNull(name) ? YOU : name) + END;
	}
}
