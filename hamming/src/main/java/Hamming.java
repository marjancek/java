public class Hamming {
	private final int hamming;

    public Hamming(String left, String right) {

    	int distance = 0;
    	
    	if(left.isEmpty() && !right.isEmpty()){
    		throw new IllegalArgumentException("left strand must not be empty.");
    	}
    	
    	if(right.isEmpty() && !left.isEmpty()){
    		throw new IllegalArgumentException("right strand must not be empty.");
    	}
    	
    	if(right.length() != left.length()){
    		throw new IllegalArgumentException("leftStrand and rightStrand must be of equal length.");
    	}

    	// We know both are the same length (constructor precondition)
    	for(int i=0; i<left.length();i++) {
    		if(left.charAt(i)!=right.charAt(i)) {
    			distance++;
    		}
    	}
    	hamming = distance;
    }

    public int getHammingDistance() {  	
    	return hamming;
    }
}
