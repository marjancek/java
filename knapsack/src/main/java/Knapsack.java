import java.util.List;

public class Knapsack {
    // from https://en.wikipedia.org/wiki/Knapsack_problem#0-1_knapsack_problem
	public int maximumValue(int capacity, List<Item> items) {
		// 0 items-> #items; 0 capacity -> capacity
		int[][] matrix = new int[items.size()+1][capacity+1];		

		// we calculate the best value for the first i-th items, with a max capacity of j
		// with zero items, or capacity of zero, the maximum value is always zero (default value for all elements)
		for(int i =1 ; i<= items.size() ; i++) {
			for(int j=1; j<=capacity; j++) {
				// with the first i elements, and a capacity of j:
				// If element heavier than current capacity, get previous best (up to i-1 elements)
				if(items.get(i-1).getWeight() > j)
					matrix[i][j] = matrix[i-1][j];
				// Otherwise, get the maximum between the previous result, 
				// and the best for i minus the current-weight, plus the current value
				else
					matrix[i][j] = Math.max(matrix[i-1][j], matrix[i-1][j-items.get(i-1).getWeight()] + items.get(i-1).getValue());
			}
		}
		return matrix[items.size()][capacity];
	}
}