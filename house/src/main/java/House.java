import java.util.ArrayList;
import java.util.List;

class House {
	
	class Sentence {
		public final String subject;
		public final String verb;
		public Sentence(String noun, String verb) {
			this.subject = noun;
			this.verb = verb;
		}
	}
	
	private List<Sentence> pairs = new ArrayList<>();
	
	public House() {
		pairs.add(new Sentence("malt", "lay in"));
		pairs.add(new Sentence("rat", "ate"));
		pairs.add(new Sentence("cat", "killed"));
		pairs.add(new Sentence("dog", "worried"));
		pairs.add(new Sentence("cow with the crumpled horn", "tossed"));
		pairs.add(new Sentence("maiden all forlorn", "milked"));
		pairs.add(new Sentence("man all tattered and torn", "kissed"));
		pairs.add(new Sentence("priest all shaven and shorn", "married"));
		pairs.add(new Sentence("rooster that crowed in the morn", "woke"));
		pairs.add(new Sentence("farmer sowing his corn", "kept"));
		pairs.add(new Sentence("horse and the hound and the horn", "belonged to"));
	}

	public String verse(int start) {
		StringBuilder builder = new StringBuilder();
		builder.append("This is");
		int ndx = start - 1; // i is 1 based; List is zero based
		while(0<=--ndx) {
			builder.append(" the ");
			builder.append(pairs.get(ndx).subject);
			builder.append(" that ");
			builder.append(pairs.get(ndx).verb);
		}
		builder.append(" the house that Jack built.");

		return builder.toString();
	}
	
	public String verses(int start, int end) {
		StringBuilder builder = new StringBuilder();
		for(int i=start; i<end; i++) {
			builder.append(verse(i));
				builder.append("\n");
		}
		builder.append(verse(end));
		return builder.toString();
	}
	
	public String sing() {
		return verses(1, 12);
	}
}