class Markdown {
	public String parse(String mrk) {

		boolean list = false;
		boolean paragraph = false;
		StringBuilder builder = new StringBuilder();

		String[] strs = mrk.replaceAll("([^_]*)__([^_]*)__([^_]*)", "$1<strong>$2</strong>$3")
				.replaceAll("([^_]*)_([^_]*)_([^_]*)", "$1<em>$2</em>$3").split("\n");

		for (String s : strs) {
			int h = header(s);
			boolean listLine = s.startsWith("* ");
			
			if (list && !listLine) {
				list = false;
				builder.append("</ul>");
			} else if (paragraph && (listLine || h > 0)) {
				paragraph = false;
				builder.append("</p>");
			}

			if (listLine) {
				if (!list) {
					list = true;
					builder.append("<ul>");
				}
				builder.append(String.format("<li>%s</li>", s.substring(2)));
			} else if (h > 0) {
				builder.append(String.format("<h%d>%s</h%d>",h, s.substring(h + 1), h));
			} else {
				if (!paragraph) {
					builder.append("<p>");
					paragraph = true;
				}
				builder.append(s);
			}

		} // for

		if (list)
			builder.append("</ul>");
		if (paragraph)
			builder.append("</p>");
		return builder.toString();
	}

	private int header(String s) {
		int i = 0;
		while (i < s.length() && s.charAt(i) == '#')
			i++;
		return i;
	}
}