import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Allergies {
	
	private final int score;
	
	public Allergies(int score) {
		this.score=score;
	}
	
	public boolean isAllergicTo(Allergen e) {
		return (e.getScore() & score) > 0;
	}
	
	public List<Allergen> getList() {
		return Stream.of(Allergen.values()).filter(this::isAllergicTo).collect(Collectors.toList());
	}
}