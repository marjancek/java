import java.util.Arrays;
import java.util.List;

class ResistorColor {
	
	private static final String[] colours = {"black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"};
	private static final List<String> colourList = Arrays.asList(colours);

	int colorCode(String color) {    
    	return colourList.indexOf(color);
    }

    String[] colors() {
        return colours;
    }
}
