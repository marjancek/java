import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class FoodChain{

	private List<String> firsts = Arrays.asList(
			"I know an old lady who swallowed a fly.\n",
			"I know an old lady who swallowed a spider.\nIt wriggled and jiggled and tickled inside her.\n",
			"I know an old lady who swallowed a bird.\nHow absurd to swallow a bird!\n",
			"I know an old lady who swallowed a cat.\nImagine that, to swallow a cat!\n",
			"I know an old lady who swallowed a dog.\nWhat a hog, to swallow a dog!\n",
			"I know an old lady who swallowed a goat.\nJust opened her throat and swallowed a goat!\n",
			"I know an old lady who swallowed a cow.\nI don't know how she swallowed a cow!\n",
			"I know an old lady who swallowed a horse.\nShe's dead, of course!");

	
	private List<String> seconds = Arrays.asList(
			"I don't know why she swallowed the fly. Perhaps she'll die.",
			"She swallowed the spider to catch the fly.\n",
			"She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.\n",
			"She swallowed the cat to catch the bird.\n",
			"She swallowed the dog to catch the cat.\n",
			"She swallowed the goat to catch the dog.\n",
			"She swallowed the cow to catch the goat.\n"
			);


	public String verse(int verse) {
		if(verse==8)
			return firsts.get(7);	
		return firsts.get(verse-1)+IntStream.range(0, verse).mapToObj(i->seconds.get(verse-i-1)).collect(Collectors.joining());
	}
	
	public String verses(int start, int end) {
		return IntStream.range(start, end+1).mapToObj(this::verse).collect(Collectors.joining("\n\n"));
	}

}