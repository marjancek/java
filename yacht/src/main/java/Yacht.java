import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Yacht {
	private final List<Integer> roll;
	private final YachtCategory cat;
	private final int[] dice;
	private static final Set<Integer> BIG_STRIGHT = new HashSet<>(Arrays.asList(2, 3, 4, 5, 6));
	private static final Set<Integer> SMALL_STRIGHT = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));

	Yacht(int[] dice, YachtCategory yachtCategory) {
		this.dice = dice;
		this.roll = Arrays.stream(dice).boxed().collect(Collectors.toList());
		this.cat = yachtCategory;
	}

	int score() {
		switch(cat) {
		case ONES:
			return scoreNumber(1);
		case TWOS:
			return scoreNumber(2);
		case THREES:
			return scoreNumber(3);
		case FOURS:
			return scoreNumber(4);
		case FIVES:
			return scoreNumber(5);
		case SIXES:
			return scoreNumber(6);
		case FOUR_OF_A_KIND:
			return scoreFour();
		case FULL_HOUSE:
			return scoreFullHose();
		case LITTLE_STRAIGHT:
			return scoreSmallStraight();
		case BIG_STRAIGHT:
			return scoreBigStraight();
		case CHOICE:
			return scoreChoice();
		case YACHT:
			return scoreYacht();
		default:
			return 0;
		}
	}

	int scoreYacht() {
		Set<Integer> rollSet = new HashSet<>(roll);
		return rollSet.size() == 1 ? 50 : 0;
	}

	int scoreChoice() {
		return IntStream.of(dice).sum();
	}

	int scoreBigStraight() {
		Set<Integer> rollSet = new HashSet<>(roll);
		return rollSet.equals(BIG_STRIGHT) ? 30 : 0;
	}

	int scoreSmallStraight() {
		Set<Integer> rollSet = new HashSet<>(roll);
		return rollSet.equals(SMALL_STRIGHT) ? 30 : 0;
	}

	int scoreFour() {
		Optional<Entry<Integer, Long>> search = roll.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
				.filter(e -> e.getValue() >= 4).findFirst();
		if (search.isPresent())
			return search.get().getKey() * 4;
		else
			return 0;
	}

	int scoreFullHose() {
		Map<Integer, Long> results = roll.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		if (results.size() == 2 && results.values().contains(3L))
			return scoreChoice();
		else
			return 0;
	}
	
	int scoreNumber(int number) {
		return IntStream.of(dice).filter(n->n==number).sum();
	}
}
