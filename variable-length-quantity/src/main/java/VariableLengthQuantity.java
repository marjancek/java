import java.util.LinkedList;
import java.util.List;

class VariableLengthQuantity {

	List<String> encode(List<Long> numbers) {
		LinkedList<String> list = new LinkedList<>();
		numbers.iterator().forEachRemaining(a -> list.addAll((encode(a))));
		return list;
	}

	List<String> encode(long number) {
		LinkedList<String> list = new LinkedList<>();
		list.addLast(String.format("0x%x", number & 0x7f));
		number >>= 7;
		while (number > 0) {
			long last = (number & 0x7f) | 0x80;
			list.addFirst(String.format("0x%x", last));
			number >>= 7;
		}
		return list;
	}

	List<String> decode(List<Long> bytes) {
		LinkedList<String> list = new LinkedList<>();
		long number = 0;
		boolean finished = false;
		for (int i = 0; i < bytes.size(); i++) {
			number<<=7;
			number |= bytes.get(i) & 0x7f;
			if ((bytes.get(i) & 0x80 )== 0)
			{
				list.addLast(String.format("0x%x", number));
				number = 0;
				finished = true;
			} else {
				finished = false;
			}
		}
		if(!finished)
			throw new IllegalArgumentException("Invalid variable-length quantity encoding");
		return list;
	}
}
