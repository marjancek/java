import java.util.Random;

class DnDCharacter {

	private int strength;
	private int dexterity;
	private int constitution;
	private int intelligence;
	private int wisdom;
	private int charisma;
	private int hitpoints;
	
	private final Random rand ;
	
	public DnDCharacter() {
		rand = new Random();
		strength = ability();
		dexterity = ability();
		constitution = ability();
		intelligence = ability();
		wisdom = ability();
		charisma = ability();
		hitpoints = modifier(constitution)+10;
	}
	
	
	// based on stolen idea
    int ability() {
    	// generate numbers from 1 to 6; 4 of them, sort them, skip smallest, add them
    	return rand.ints(1, 7).limit(4).sorted().skip(1).sum();
    }

    int modifier(int input) {
        return Math.floorDiv(input-10, 2);
    }

    int getStrength() {
    	return strength;
    }

    int getDexterity() {
    	return dexterity;
    }

    int getConstitution() {
    	return constitution;
    }

    int getIntelligence() {
    	return intelligence;
    }

    int getWisdom() {
    	return wisdom;
    }

    int getCharisma() {
    	return charisma;
    }

    int getHitpoints() {
        return hitpoints;
    }

}
