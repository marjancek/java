import java.time.DayOfWeek;
import java.time.LocalDate;

class Meetup {
	
	private final LocalDate start;

	public Meetup(int month, int year) {
		start = LocalDate.of(year, month, 1);
	}
	
	public LocalDate day(DayOfWeek wday, MeetupSchedule schedule) {		
		LocalDate now = start;
		switch(schedule) {
		case FIRST: now = findFrom(start, wday, 1);  break;
		case SECOND: now = findFrom(start.plusDays(7), wday, 1); break; 
		case THIRD: now = findFrom(start.plusDays(14), wday, 1);break;
		case FOURTH: now = findFrom(start.plusDays(21), wday, 1);break;
		case LAST: now = findFrom(start.plusMonths(1).plusDays(-1), wday, -1);break;
		case TEENTH: now = findFrom(start.plusDays(12), wday, 1);break;
		}
		return now;
	}
	
	public LocalDate findFrom(final LocalDate from, final DayOfWeek wday, int direction) {
		LocalDate now = from;
		while (now.getDayOfWeek() != wday)
			now = now.plusDays(direction);
		return now;
	}

/* 
 	Another option: use java.time.temporal.TemporalAdjusters.
 
    now = start.with(TemporalAdjusters.firstInMonth(wday));
    now = start.with(TemporalAdjusters.dayOfWeekInMonth(2 ,wday));
    now = start.with(TemporalAdjusters.dayOfWeekInMonth(3, wday));
    now = start.with(TemporalAdjusters.dayOfWeekInMonth(4, wday));
    now = start.with(TemporalAdjusters.lastInMonth(wday));
    now = start.with(TemporalAdjusters.next(wday));
*/
	
}
